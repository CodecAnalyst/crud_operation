package com.crud.controller;

import com.crud.model.User;

import com.crud.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
public class UserController {

    @Autowired
    UserRepository userRepository;


    /**
     *
     * @return return all the user registered
     */
    @GetMapping("/getUsers")
    public List<User> retrieveAllStudents() {

        return userRepository.findAll();
    }

    /**
     *
     * @param id of the user register
     * @return return object representation of the user
     * @throws Exception
     */

    @RequestMapping(value = "/getUser",method = RequestMethod.POST)
    public User retrieveUser(@RequestParam long id) throws Exception {
        Optional<User> user = userRepository.findById(id);

        if (!user.isPresent())
            throw new Exception("id-" + id);

        return user.get();
    }

    /**
     *
     * @param Id of the user register for delete
     * @throws Exception
     */
    @DeleteMapping(value = "/deleteUser")
    public void deleteUser(@RequestParam long Id) throws Exception{

        userRepository.deleteById(Id);
    }

    /**
     *
     * @param userRequest object of the user to be register
     * @return entity URl
     */
    @PostMapping("/createUser")
    public ResponseEntity<Object> createUser(@RequestBody User userRequest) {
        User savedUser = userRepository.save(userRequest);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(userRequest.getId()).toUri();

        return ResponseEntity.created(location).build();

    }

    /**
     *
     * @param user object to update
     * @param id of the register user to search
     * @return Entity Url
     */
    @PutMapping("/updateUser/{id}")
    public ResponseEntity<Object> updateUser(@RequestBody User user, @PathVariable long id) {

        Optional<User> userOptional = userRepository.findById(id);

        if (!userOptional.isPresent())
            return ResponseEntity.notFound().build();

        user.setId(id);

        userRepository.save(user);

        return ResponseEntity.noContent().build();
    }
}
